﻿using UnityEngine;
namespace Grid
{ 
    [RequireComponent(typeof(GridItemViem))]
    [RequireComponent(typeof(BoxCollider))]
    public class GridItem : MonoBehaviour, IMouseClick
    {

        public bool IsFull { get; private set; }
        public bool IsReadyToMove { get; private set; }
        public int NumPlayerFull { get; private set; }
        public int myX { get; private set; }
        public int myY { get; private set; }

        private GridItemViem itemViem;

        public void Init(int _x, int _y)
        {
            itemViem = GetComponent<GridItemViem>();
            myX = _x;
            myY = _y;
        }

        public void ClearCell()
        {
            NumPlayerFull = -1;
            IsReadyToMove = false;
            IsFull = false;
            itemViem.ClearCell();
        }

        public void ReplaceCell()
        {
            IsReadyToMove = true;
            itemViem.ReadyToMoveCell();
        }

        public void FillCell(Color _color, int numPlayer)
        {
            NumPlayerFull = numPlayer;
            IsReadyToMove = false;
            IsFull = true;
            itemViem.FillCell(_color);
        }     

        public void OnMouseDown()
        {
            TurnManager.Instance.TryFillCell(this);
        }
    }
}
