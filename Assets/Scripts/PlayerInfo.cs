﻿using System;
using UnityEngine;

[Serializable]
public struct PlayerInfo
{
    public string NamePlayer;   
    public Color colorCell;
    public int CountTurn;
}

[Serializable]
public class PlayersData
{
    public PlayerInfo[] PlayerInfo;
}


