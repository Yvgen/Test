﻿using UnityEngine;
using UnityEngine.UI;

public class GamePlayUI : UI
{
    [SerializeField]
    private Text textPlayerName;
    [SerializeField]
    private Text textWinner;

    public void SetCurrentPlayerName(string _name)
    {
        FillText(textPlayerName, _name);        
    }

    public void ClearaTextWinner()
    {
        ClearText(textWinner);       
    }

    public void SetTextWinner(string _name)
    {
        FillText(textWinner, "winner " + _name);
    }
}
