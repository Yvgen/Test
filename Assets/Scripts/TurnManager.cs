﻿using UnityEngine;
using Grid;

public class TurnManager : Singleton<TurnManager> {   

    private GridManager gridManager;
    private GamePlayUI gamePlayUI;  
    private PlayersData PlayersData = new PlayersData();
    private int currentPlayerTurn, countTurns;
    private const int maxTurns = 6;
    private bool canClick;
    private GridItem swapCell;
 
    void Start()
    {  
        string fromJson = Resources.Load<TextAsset>("Players").text;
        PlayersData =JsonUtility.FromJson< PlayersData >(fromJson);            
        gamePlayUI = FindObjectOfType<GamePlayUI>();
        gridManager = FindObjectOfType<GridManager>();
        StartGame();
    }

    public void TryFillCell(GridItem _cell)
    {
        if (!canClick)
            return;

        if (countTurns <= maxTurns)
        {
            if (!_cell.IsFull)
                FillCell(_cell);
        }
        else
        {
            ReplaceCell(_cell);
        }
    }

    private void FillCell(GridItem _cell)
    {
        _cell.FillCell(GetCurrentPlayerColor(), currentPlayerTurn);
        NextPlayerTurn();
    }

    private void ReplaceCell(GridItem _cell)
    {
        if (swapCell == null && _cell.IsFull && _cell.NumPlayerFull == currentPlayerTurn)
        {
            swapCell = _cell;
            _cell.ReplaceCell();
        }
        else if (swapCell != null && !_cell.IsFull)
        {
            if(IsNeighbor(_cell))
            {
                swapCell.ClearCell();
                swapCell = null;
                FillCell(_cell);               
            }
        }
        else if (swapCell != null && _cell.IsFull && _cell.NumPlayerFull == currentPlayerTurn)
        {
            swapCell.FillCell(GetCurrentPlayerColor(), currentPlayerTurn);
            swapCell = _cell;
            _cell.ReplaceCell();
        }
    }

    private Color GetCurrentPlayerColor()
    {
        return PlayersData.PlayerInfo[currentPlayerTurn].colorCell;
    }

    private bool IsNeighbor(GridItem _cell)
    {
        if (swapCell.myX == _cell.myX + 1 && swapCell.myY == _cell.myY || swapCell.myX == _cell.myX - 1 && swapCell.myY == _cell.myY ||
            swapCell.myX == _cell.myX && swapCell.myY == _cell.myY + 1 || swapCell.myX == _cell.myX && swapCell.myY == _cell.myY - 1 ||
            swapCell.myX == _cell.myX + 1 && swapCell.myY == _cell.myY + 1 || swapCell.myX == _cell.myX - 1 && swapCell.myY == _cell.myY - 1 ||
            swapCell.myX == _cell.myX - 1 && swapCell.myY == _cell.myY + 1 || swapCell.myX == _cell.myX + 1 && swapCell.myY == _cell.myY - 1
            )
            return true;
        else
            return false;
    }

    private bool CheckForWinner()
    {
        int winnerNum = gridManager.GetWinnerPlayer();
        if (winnerNum >= 0)
        {
            canClick = false;
            gamePlayUI.SetTextWinner(PlayersData.PlayerInfo[winnerNum].NamePlayer);
            Invoke("StartGame", 2);
            return true;
        }
        else        
            return false;        
    }

    private void StartGame()
    {
        canClick = true;
        gamePlayUI.ClearaTextWinner();          
        gridManager.ClearGrid();
        countTurns = 0;
        currentPlayerTurn = -1;
        NextPlayerTurn();
    }

    private void NextPlayerTurn()
    {
        if (!CheckForWinner())
        {
            countTurns++;
            currentPlayerTurn++;
            if (currentPlayerTurn >= PlayersData.PlayerInfo.Length)
                currentPlayerTurn = 0;
            gamePlayUI.SetCurrentPlayerName(PlayersData.PlayerInfo[currentPlayerTurn].NamePlayer);
        }
    }

}
