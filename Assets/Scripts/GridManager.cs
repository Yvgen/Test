﻿using UnityEngine;
namespace Grid
{
    public class GridManager : MonoBehaviour
    {  
        private const int lineCountX = 3, lineCountY = 3;
        private const float startX = -2f, startY = 2f, distance = 2f;
        private GridItem[,] gridItems = new GridItem[lineCountX, lineCountY];

        // Use this for initialization
        void Awake()
        {           
            CreateGrid();
        }

        public void ClearGrid()
        {
            for (int x = 0; x < lineCountX; x++)
            {
                for (int y = 0; y < lineCountY; y++)
                {
                    gridItems[x, y].ClearCell();
                }
            }
        }

        public int GetWinnerPlayer()
        {
            for (int x = 0; x < lineCountX; x++)
            {
                if (gridItems[x, 0].NumPlayerFull == gridItems[x, 1].NumPlayerFull && gridItems[x, 1].NumPlayerFull == gridItems[x, 2].NumPlayerFull)
                    return gridItems[x, 0].NumPlayerFull;
            }
            for (int y = 0; y < lineCountY; y++)
            {
                if (gridItems[0, y].NumPlayerFull == gridItems[1, y].NumPlayerFull && gridItems[1, y].NumPlayerFull == gridItems[2, y].NumPlayerFull)
                    return gridItems[0, y].NumPlayerFull;
            }
            if (gridItems[0, 0].NumPlayerFull == gridItems[1, 1].NumPlayerFull && gridItems[1, 1].NumPlayerFull == gridItems[2, 2].NumPlayerFull)
                return gridItems[0, 0].NumPlayerFull;
            if (gridItems[0, 2].NumPlayerFull == gridItems[1, 1].NumPlayerFull && gridItems[1, 1].NumPlayerFull == gridItems[2, 0].NumPlayerFull)
                return gridItems[0, 2].NumPlayerFull;

            return -1;
        }     

        private void CreateGrid()
        {
            GameObject prefabGridItem = Resources.Load<GameObject>("GridItem");
            float posX = startX;
            float posY = startY;
            for (int x = 0; x < lineCountX; x++)
            {
                for (int y = 0; y < lineCountY; y++)
                {
                    GameObject _clone = Instantiate(prefabGridItem, transform);
                    gridItems[x, y] = _clone.GetComponent<GridItem>();
                    gridItems[x, y].Init(x, y);
                    _clone.transform.position = new Vector3(posX, posY, 0);
                    posY -= distance;
                }
                posY = startY;
                posX += distance;
            }
        }
    }
}
