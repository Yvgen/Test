﻿using UnityEngine;
namespace Grid
{
    public class GridItemViem : MonoBehaviour
    {
        private SpriteRenderer mySprite;
        private Color clearColor = Color.white;
        private const float alfaGradient = 0.8f;

        // Use this for initialization
        void Start()
        {
            mySprite = transform.Find("Sprite").GetComponent<SpriteRenderer>();
        }

        public void ClearCell()
        {           
            mySprite.color = ChangeColor(clearColor, alfaGradient);
        }

        public void ReadyToMoveCell()
        {          
            mySprite.color = ChangeColor(mySprite.color, 1); 
        }

        public void FillCell(Color _colorFill)
        {         
            mySprite.color = ChangeColor(_colorFill, alfaGradient);
        }

        private Color ChangeColor(Color _color , float _alfa)
        {
            Color colorGradient = _color;
            colorGradient.a = _alfa;
            return colorGradient;
        }
    }
}
