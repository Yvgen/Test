﻿using UnityEngine;
using UnityEngine.UI;
public class UI : MonoBehaviour
{
    public void ClearText(Text _text)
    {
        _text.text = "";
    }

    public void FillText(Text _text, string _value)
    {
        _text.text = _value;
    }
}
